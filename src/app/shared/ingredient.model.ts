export class Ingredient {
  constructor(
    public name: string,
    public quantity = 0,
    public price: number,
    public image: string,
  ) {}

  getPrice() {
    return this.price * this.quantity;
  }
}
