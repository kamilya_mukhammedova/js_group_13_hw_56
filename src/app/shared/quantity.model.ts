export class Quantity {
  constructor(
    public name: string,
    public quantity = 0,
  ) {}
}
