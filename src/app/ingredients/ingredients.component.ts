import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent {
 @Input() nameOfIngredient = '';
 @Input() quantityOfIngredient = 0;
 @Input() imageOfIngredient = '';
 @Output() clickedIngredient = new EventEmitter();
 @Output() deletedIngredient = new EventEmitter();

  getClickToIngredient() {
    this.clickedIngredient.emit();
  }

  delete() {
    this.deletedIngredient.emit();
  }
}
