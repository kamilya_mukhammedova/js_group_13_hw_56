import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-filling',
  templateUrl: './filling.component.html',
  styleUrls: ['./filling.component.css']
})
export class FillingComponent {
 @Input() filling = '';
 @Input() isOnPage = false;
}
